use std::fs;

use rand::{thread_rng, Rng};
use serenity::async_trait;

use serenity::model::channel::{Message, Reaction};
use serenity::model::gateway::Ready;

use serenity::prelude::*;

use crate::interaction_parser::{
    del_trigger_parser, echo_parser, make_trigger_parser, ping_parser, show_triggers_parser,
    shutdown_parser, starboard_parser, suggest_parser, test_parser,
};
use crate::util::get_avatar_endpoint;

use crate::util::get_colour;
use crate::{LIST_OF_RESPONSES_TO_PAT, TRIGGERS_FOR_PATTER_BLUSH};
use chrono::Utc;

use serenity::model::interactions::application_command::{
    ApplicationCommand, ApplicationCommandOptionType,
};

use serenity::model::interactions::{Interaction, InteractionResponseType};

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, _ctx: Context, _new_message: Message) {
        if _new_message.author.bot {
            return;
        }

        let content_lower = _new_message.content.to_lowercase();
        let mut content_lower_trim = content_lower.trim().to_owned();
        if content_lower_trim.starts_with("<@!863070612360265778> ") {
            if content_lower_trim.starts_with("<@!863070612360265778> me") {
                content_lower_trim =
                    content_lower_trim.replace("<@!863070612360265778> me", "pat me");
            } else {
                content_lower_trim = content_lower_trim.replace("<@!863070612360265778> ", "");
            }
        } //Making it work with getting tagged in all forms tagged ex. "@PAT good bot", "@PAT pat me", "@PAT me"

        let dictionary_public =
            json::parse(&*fs::read_to_string("responses.json").unwrap()).unwrap();
        //Optimally this shouldn't be done this way, it probably slows down the bot loads to constantly for every message access disk

        if TRIGGERS_FOR_PATTER_BLUSH.contains(&&*content_lower_trim) {
            let index: usize;

            {
                let mut rng = thread_rng();
                index = rng.gen_range(0..LIST_OF_RESPONSES_TO_PAT.len());
            }

            let _test = _new_message
                .reply(_ctx, LIST_OF_RESPONSES_TO_PAT[index])
                .await;
        } else if dictionary_public.has_key(&*content_lower_trim) {
            let dictionary_personal =
                json::parse(&*fs::read_to_string("personal_user_responses.json").unwrap()).unwrap();

            let roll: bool;

            {
                let mut rng = thread_rng();
                roll = rng.gen_bool(0.5_f64);
            }

            if roll && dictionary_personal.has_key(&_new_message.author.id.to_string()) {
                let _ = _new_message
                    .reply(
                        _ctx,
                        dictionary_personal[_new_message.author.id.to_string()].clone(),
                    )
                    .await;
            } else {
                let response = dictionary_public[content_lower_trim].clone();
                if response.is_array() {
                    let index: usize;

                    {
                        let mut rng = thread_rng();
                        index = rng.gen_range(0..response.len());
                    }

                    let _ = _new_message.reply(_ctx, response[index].clone()).await;
                } else {
                    let _ = _new_message.reply(_ctx, response.clone()).await;
                }
            }
        }
    }

    async fn reaction_add(&self, _ctx: Context, _add_reaction: Reaction) {
        if _add_reaction
            .emoji
            .to_string()
            .ne("<:admire:862678848008355860>")
        {
            return;
        }

        let msg = _add_reaction.message(&_ctx).await.unwrap();

        let reactions = msg
            .reactions
            .iter()
            .find(|item| item.reaction_type == _add_reaction.emoji)
            .unwrap();

        if reactions.count < 2 {
            return;
        }

        #[allow(clippy::unreadable_literal)]
        let channel_result = &_ctx.cache.channel(863066429997318154).await;
        let channel = if let Some(x) = channel_result {
            x
        } else {
            return;
        };

        let guild_channel_result = channel.clone().guild();
        let guild_channel = if let Some(x) = guild_channel_result {
            x
        } else {
            let _ = msg
                .reply(&_ctx, "Couldn't find a channel with that id")
                .await;
            return;
        };

        let temp1 = fs::read_to_string("starboard_opted_out_users.txt").unwrap();
        let temp2 = fs::read_to_string("starboarded_messages.txt").unwrap();
        let opted_out_users_contains = temp1
            .split('|')
            .any(|element| element == msg.author.id.to_string());
        let already_starboarded_contains = temp2
            .split('|')
            .any(|element| element == msg.id.to_string());

        if opted_out_users_contains | already_starboarded_contains {
            return;
        }

        let guild_option = guild_channel.guild(&_ctx).await;
        let guild = if let Some(x) = guild_option {
            x
        } else {
            return;
        };

        let guild_member = guild.member(&_ctx, &msg.author.id).await.unwrap();
        let role_option = guild_member.highest_role_info(&_ctx).await;

        let role_colour = get_colour(role_option, &guild);

        let time = msg.timestamp as chrono::DateTime<Utc>;
        let member_name = &guild_member.display_name();
        let avatar_endpoint = get_avatar_endpoint(&guild_member, &guild);

        let _ = guild_channel
            .send_message(_ctx, |m| {
                m.embed(|e| {
                    e.color(role_colour)
                        .field("**Admired message:**", &msg.content, false)
                        .timestamp(time.to_rfc3339())
                        .title("New admired message!!")
                        .url(msg.link())
                        .author(|author| author.name(member_name).icon_url(avatar_endpoint));

                    if !&msg.attachments.is_empty() {
                        e.attachment(msg.attachments[0].clone().url);
                    }
                    e
                })
            })
            .await;

        let mut file_contents = fs::read_to_string("starboarded_messages.txt").unwrap();
        file_contents.push_str(&*("|".to_owned() + &*msg.id.to_string()));

        let _ = fs::write("starboarded_messages.txt", file_contents);
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);

        #[allow(clippy::unreadable_literal)]
            let _ = ApplicationCommand::set_global_application_commands(&ctx.http, |commands| {
            commands
                    .create_application_command(|command| {
                        command.name("ping").description("Get bot latency")
                    })
                    .create_application_command(|command| {
                        command.name("test").description("Simple test command")
                    })
                    .create_application_command(|command| {
                        command
                            .name("echo")
                            .description("Echo given text into channel specified")
                            .create_option(|option| {
                                option
                                    .name("safe_pass")
                                    .description(
                                        "Whether to turn role mentions into non mentioning ones",
                                    )
                                    .kind(ApplicationCommandOptionType::Boolean)
                                    .required(true)
                            })
                            .create_option(|option| {
                                option
                                    .name("id")
                                    .description("Channel ID where to post")
                                    .kind(ApplicationCommandOptionType::Channel)
                                    .required(true)
                            })
                            .create_option(|option| {
                                option
                                    .name("text")
                                    .description("Text to echo into channel")
                                    .kind(ApplicationCommandOptionType::String)
                                    .required(true)
                            })
                    })
                    .create_application_command(|command| {
                        command
                            .name("del_trigger")
                            .description("Delete a text based trigger")
                            .create_option(|option| {
                                option
                                    .name("key")
                                    .description("Which trigger to delete")
                                    .kind(ApplicationCommandOptionType::String)
                                    .required(true)
                            })
                            .create_option(|option| {
                                option
                                    .name("public_bool")
                                    .description("Specify which kind of trigger")
                                    .kind(ApplicationCommandOptionType::Boolean)
                                    .required(true)
                            })
                    })
                    .create_application_command(|command| {
                        command
                            .name("show_triggers")
                            .description("Show a given list of text based triggers")
                            .create_option(|option| {
                                option
                                    .name("public_bool")
                                    .description("Specify which kind of trigger")
                                    .kind(ApplicationCommandOptionType::Boolean)
                                    .required(true)
                            })
                    })
                    .create_application_command(|command| {
                        command
                            .name("make_trigger")
                            .description("Create a text based trigger")
                            .create_option(|option| {
                                option
                                    .name("key")
                                    .description("Key of trigger to add")
                                    .kind(ApplicationCommandOptionType::String)
                                    .required(true)
                            })
                            .create_option(|option| {
                                option
                                    .name("response")
                                    .description("What should the bot respond")
                                    .kind(ApplicationCommandOptionType::String)
                                    .required(true)
                            })
                            .create_option(|option| {
                                option
                                    .name("public_bool")
                                    .description("Specify which kind of trigger")
                                    .kind(ApplicationCommandOptionType::Boolean)
                                    .required(true)
                            })
                            .create_option(|option| {
                                option
                                    .name("is_list")
                                    .description("Whether response is one continuous thing or a list of responses")
                                    .kind(ApplicationCommandOptionType::Boolean)
                                    .required(true)
                            })
                    })
                    .create_application_command(|command| {
                        command
                            .name("suggest")
                            .description("Suggest an addition/change to the server").create_option(|option| {
                            option
                                .name("suggest")
                                .description("What should be suggested")
                                .kind(ApplicationCommandOptionType::String)
                                .required(true)
                        })
                    })
                    .create_application_command(|command| {
                        command
                            .name("starboard")
                            .description("Opt in or out to your messages getting on the starboard").create_option(|option| {
                            option
                                .name("in_or_out")
                                .description("Whether you're trying to opt in or out, leave blank to flip preference")
                                .kind(ApplicationCommandOptionType::String)
                                .add_string_choice("in", "in")
                                .add_string_choice("out", "out")
                        })
                    })
                    .create_application_command(|command| {
                        command.name("shutdown").description("Shutdown the bot, will always \"fail\" since the bot can't respond")
                    })
            })
            .await;
    }

    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            let content = match command.data.name.as_str() {
                "test" => test_parser(),
                "ping" => ping_parser(&ctx).await,
                "echo" => {
                    let command = command.clone();
                    echo_parser(command, &ctx).await
                }
                "del_trigger" => {
                    let command = command.clone();
                    del_trigger_parser(command, &ctx).await
                }
                "make_trigger" => {
                    let command = command.clone();
                    make_trigger_parser(&command)
                }
                "show_triggers" => {
                    let command = command.clone();
                    show_triggers_parser(&command)
                }
                "shutdown" => {
                    let command = command.clone();
                    shutdown_parser(command, &ctx).await
                }
                "suggest" => {
                    let command = command.clone();
                    suggest_parser(command, &ctx).await
                }
                "starboard" => {
                    let command = command.clone();
                    starboard_parser(command, &ctx).await
                }
                _ => "not implemented :(".to_string(),
            };

            if let Err(why) = command
                .create_interaction_response(&ctx.http, |response| {
                    response
                        .kind(InteractionResponseType::ChannelMessageWithSource)
                        .interaction_response_data(|message| message.content(content))
                })
                .await
            {
                println!("Cannot respond to slash command: {}", why);
            }
        } else {
            println!("Unhandled interaction: {:?}", interaction);
        }
    }
}
