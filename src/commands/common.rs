use crate::util::{get_avatar_endpoint, get_colour};
use crate::ShardManagerContainer;
use chrono::Utc;
use json::JsonValue;
use serenity::client::bridge::gateway::ShardId;
use serenity::model::id::UserId;
use serenity::utils::{content_safe, ContentSafeOptions};
use serenity::{
    framework::standard::{
        macros::{command, group},
        Args, CommandResult,
    },
    model::channel::Message,
    prelude::*,
};
use std::fs;

#[group("administrative")]
#[owners_only]
#[description = "Administrative commands for interacting with the bot"]
#[commands(echo, del_trigger, show_triggers, make_trigger, shutdown)]
pub struct Administrative;

#[command]
async fn del_trigger(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let trigger_key_res = args.single::<String>();
    let public_bool_res = args.single::<bool>();

    if trigger_key_res.is_err() | public_bool_res.is_err() {
        msg.reply(ctx, "Couldn't get trigger name or public/private boolean")
            .await?;
        return Ok(());
    }

    let trigger_key = trigger_key_res.unwrap();
    let public_bool = public_bool_res.unwrap();

    let mut responses = if public_bool {
        json::parse(&*fs::read_to_string("responses.json").unwrap()).unwrap()
    } else {
        json::parse(&*fs::read_to_string("personal_user_responses.json").unwrap()).unwrap()
    };

    if !responses.has_key(&*trigger_key) {
        msg.reply(ctx, "Such a trigger doesn't exist in the list specified")
            .await?;
        return Ok(());
    }

    msg.reply(
        ctx,
        format!(
            "Are you sure you want to delete the trigger {}? (Y/N)",
            trigger_key
        ),
    )
    .await?;

    let answer = msg.author.await_reply(&ctx).await;
    if let Some(message) = answer {
        return match &*message.content {
            "Y" => {
                responses.remove(&*trigger_key);

                if public_bool {
                    let _ = fs::write("responses.json", responses.dump());
                } else {
                    let _ = fs::write("personal_user_responses.json", responses.dump());
                }
                msg.reply(ctx, format!("Done! {} was deleted", trigger_key))
                    .await?;
                Ok(())
            }
            "N" => {
                msg.reply(&ctx, "Canceled.").await?;
                Ok(())
            }
            _ => {
                msg.reply(&ctx, "Unknown input").await?;
                Ok(())
            }
        };
    }

    Ok(())
}

#[command]
async fn make_trigger(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let trigger_key_res = args.single::<String>();
    let response_res = args.single_quoted::<String>();
    let public_bool_res = args.single::<bool>();
    let is_list_res = args.single::<bool>();

    if trigger_key_res.is_err()
        | public_bool_res.is_err()
        | response_res.is_err()
        | is_list_res.is_err()
    {
        msg.reply(ctx, "Couldn't get all required arguments")
            .await?;
        return Ok(());
    }

    let trigger_key = trigger_key_res.unwrap();
    let response = response_res.unwrap();
    let public_bool = public_bool_res.unwrap();
    let is_list = is_list_res.unwrap();

    let file_name = if public_bool {
        "responses.json"
    } else {
        "personal_user_responses.json"
    };

    let mut dict_triggers = json::parse(&*fs::read_to_string(file_name).unwrap()).unwrap();
    if is_list {
        dict_triggers[trigger_key.replace('+', " ")] =
            JsonValue::from(response.replace('+', " ").split(", ").collect::<Vec<_>>());
    } else {
        dict_triggers[trigger_key.replace('+', " ")] = JsonValue::from(response.replace('+', " "));
    }

    let _ = fs::write(file_name, dict_triggers.dump());

    msg.reply(
        ctx,
        format!("Set trigger {} with response {}", trigger_key, response),
    )
    .await?;

    Ok(())
}

#[command]
async fn show_triggers(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let pub_private = if let Ok(x) = args.single::<bool>() {
        x
    } else {
        msg.reply(ctx, "Couldn't get the public/private argument")
            .await?;
        return Ok(());
    };
    let file_name = if pub_private {
        "responses.json"
    } else {
        "personal_user_responses.json"
    };

    msg.reply(
        ctx,
        "```json\n".to_owned()
            + &json::parse(&*fs::read_to_string(file_name).unwrap())
                .unwrap()
                .pretty(4)
            + "```",
    )
    .await?;

    Ok(())
}

#[command]
async fn echo(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let channel_id_result = args.single::<u64>();
    let channel_id = if let Ok(x) = channel_id_result {
        x
    } else {
        msg.reply(ctx, "Not a valid ID format").await?;
        return Ok(());
    };

    let channel_result = ctx.cache.channel(channel_id).await;
    let channel = if let Some(x) = channel_result {
        x
    } else {
        msg.reply(ctx, "Couldn't find a channel with that id")
            .await?;
        return Ok(());
    };

    let guild_channel_result = channel.guild();
    let guild_channel = if let Some(x) = guild_channel_result {
        x
    } else {
        msg.reply(ctx, "Couldn't find a channel with that id")
            .await?;
        return Ok(());
    };

    let do_safe_passing_result = args.single::<bool>();
    let do_safe_passing = if let Ok(x) = do_safe_passing_result {
        x
    } else {
        msg.reply(ctx, "Couldn't parse safe passing bool").await?;
        return Ok(());
    };

    if do_safe_passing {
        let safe_text = content_safe(
            &ctx,
            args.rest(),
            &ContentSafeOptions::new()
                .clean_everyone(true)
                .clean_here(true)
                .clean_role(true)
                .clean_channel(false)
                .clean_user(false),
        )
        .await;

        guild_channel.say(ctx, safe_text).await?;
    } else {
        guild_channel.say(ctx, args.rest()).await?;
    }

    Ok(())
}

//noinspection DuplicatedCode
#[command]
#[aliases("stop")]
async fn shutdown(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;

    let shard_manager = if let Some(v) = data.get::<ShardManagerContainer>() {
        v
    } else {
        msg.reply(ctx, "There was a problem getting the shard manager")
            .await?;

        return Ok(());
    };

    println!(
        "Shutting down; requested by {} ({})",
        msg.author.name, msg.author.id
    );

    shard_manager.lock().await.shutdown_all().await;

    Ok(())
}

#[group("user")]
#[description = "Commands intended for the end user"]
#[commands(ping, test, suggest, starboard)]
pub struct User;

//noinspection DuplicatedCode
#[command]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;

    let shard_manager = if let Some(v) = data.get::<ShardManagerContainer>() {
        v
    } else {
        msg.reply(ctx, "There was a problem getting the shard manager")
            .await?;

        return Ok(());
    };
    let manager = shard_manager.lock().await;

    let runners = manager.runners.lock().await;

    // Shards are backed by a "shard runner" responsible for processing events
    // over the shard, so we'll get the information about the shard runner for
    // the shard this command was sent over.
    let runner = if let Some(runner) = runners.get(&ShardId(ctx.shard_id)) {
        runner
    } else {
        msg.reply(ctx, "No shard found???").await?;

        return Ok(());
    };

    let latency = if let Some(x) = runner.latency {
        x
    } else {
        msg.reply(ctx, "Couldn't get latency, usually due to fresh start")
            .await?;
        return Ok(());
    };

    msg.reply(
        ctx,
        &format!("The shard latency is {}ms", latency.as_millis()),
    )
    .await?;

    Ok(())
}

#[command]
async fn test(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "hiiiiii").await?;

    Ok(())
}

#[command]
async fn suggest(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    #[allow(clippy::unreadable_literal)]
    let channel_result = ctx.cache.channel(871400397225488404).await;
    let channel = if let Some(x) = channel_result {
        x
    } else {
        msg.reply(ctx, "Couldn't find suggestions channel").await?;
        return Ok(());
    };

    let guild_channel_result = channel.guild();
    let guild_channel = if let Some(x) = guild_channel_result {
        x
    } else {
        msg.reply(ctx, "Couldn't find a channel with that id")
            .await?;
        return Ok(());
    };

    let guild_option = guild_channel.guild(ctx).await;
    let guild = if let Some(x) = guild_option {
        x
    } else {
        msg.reply(ctx, "Couldn't get guild").await?;
        return Ok(());
    };

    let guild_member = guild.member(ctx, msg.author.id).await?;
    let role_option = guild_member.highest_role_info(ctx).await;

    let role_colour = get_colour(role_option, &guild);

    let time = msg.timestamp as chrono::DateTime<Utc>;
    let member_name = &guild_member.display_name();
    let avatar_endpoint = get_avatar_endpoint(&guild_member, &guild);

    //println!("{}", avatar_endpoint);
    //println!("{:?}", avatar_hash);

    guild_channel
        .send_message(ctx, |m| {
            m.embed(|e| {
                e.color(role_colour)
                    .field("**Suggestion:**", args.rest(), false)
                    .timestamp(time.to_rfc3339());

                e.thumbnail(avatar_endpoint)
            })
            .content(format!(
                "Sent by {} in {}",
                member_name,
                guild_channel.mention()
            ))
        })
        .await?;

    Ok(())
}
#[command]
async fn starboard(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let option = args.single::<String>();

    let opted_out_users = fs::read_to_string("starboard_opted_out_users.txt").unwrap();
    let already_opted_out = opted_out_users
        .split('|')
        .any(|user| user == msg.author.id.to_string());

    if let Ok(x) = option {
        match &*x.to_lowercase() {
            "in" => {
                if !already_opted_out {
                    msg.reply(&ctx, "You're not opted out").await?;
                    return Ok(());
                }
                msg.reply(&ctx, "Are you sure you want to opt back in? (Y/N)")
                    .await?;
                let answer = msg.author.await_reply(&ctx).await;
                if let Some(message) = answer {
                    process_reply(&message, already_opted_out);
                    return Ok(());
                };
            }
            "out" => {
                if already_opted_out {
                    msg.reply(&ctx, "You're not opted in").await?;
                    return Ok(());
                }
                msg.reply(&ctx, "Your messages can currently be put on the starboard. Do you want to opt out? (Y/N)").await?;

                let answer = msg.author.await_reply(&ctx).await;
                if let Some(message) = answer {
                    process_reply(&message, already_opted_out);
                    return Ok(());
                };
            }
            _ => {
                msg.reply(
                    &ctx,
                    "Unknown input. Specify either 'in' or 'out', or nothing to flip preference",
                )
                .await?;
                return Ok(());
            }
        }
    } else {
        if already_opted_out {
            msg.reply(ctx, "Are you sure you want to opt back in? (Y/N)")
                .await?;
        } else {
            msg.reply(ctx, "Your messages can currently be put on the starboard. Do you want to opt out? (Y/N)")
                .await?;
        }

        let answer = msg.author.await_reply(&ctx).await;
        if let Some(message) = answer {
            process_reply(&message, already_opted_out);
            return Ok(());
        }
    };

    Ok(())
}

pub fn process_reply(msg: &Message, opt_out: bool) -> String {
    return match &*msg.content {
        "Y" => {
            let mut file_contents = fs::read_to_string("starboard_opted_out_users.txt").unwrap();

            if opt_out {
                let users = file_contents.split('|');
                let all_other_users_filter =
                    users.filter(|user| user != &&*msg.author.id.to_string());
                let all_other_users: Vec<_> = all_other_users_filter.collect();
                file_contents = all_other_users.join("|");
            } else {
                file_contents.push_str(&*("|".to_owned() + &*msg.author.id.to_string()));
            };

            let _ = fs::write("starboard_opted_out_users.txt", file_contents);
            "Done!".to_owned()
        }
        "N" => "Canceled.".to_owned(),
        _ => "Unknown input.".to_owned(),
    };
}

pub fn process_reply_no_affirm(author_id: UserId, opt_out: bool) -> String {
    let mut file_contents = fs::read_to_string("starboard_opted_out_users.txt").unwrap();

    if opt_out {
        let users = file_contents.split('|');
        let all_other_users_filter = users.filter(|user| user != &&*author_id.to_string());
        let all_other_users: Vec<_> = all_other_users_filter.collect();
        file_contents = all_other_users.join("|");
    } else {
        file_contents.push_str(&*("|".to_owned() + &*author_id.to_string()));
    };

    let _ = fs::write("starboard_opted_out_users.txt", file_contents);
    "Done!".to_owned()
}
