#[allow(unused_imports)]
use crate::commands::common::{process_reply, process_reply_no_affirm};
use crate::util::{get_avatar_endpoint, get_colour};
use crate::ShardManagerContainer;
use chrono::Utc;
use json::JsonValue;
use serenity::client::bridge::gateway::ShardId;

use serenity::model::id::ChannelId;
use serenity::model::interactions::InteractionResponseType;
use serenity::model::prelude::application_command::ApplicationCommandInteraction;
use serenity::model::prelude::User;
use serenity::prelude::*;
use serenity::utils::{content_safe, ContentSafeOptions};
use std::fs;
use std::option::Option::Some;

pub fn del_trigger(
    /*
    ctx: &Context,
    msg_channel: ChannelId,
    author: User,
    */
    trigger_key: String,
    public_bool: bool,
) -> String {
    let mut responses = if public_bool {
        json::parse(&*fs::read_to_string("responses.json").unwrap()).unwrap()
    } else {
        json::parse(&*fs::read_to_string("personal_user_responses.json").unwrap()).unwrap()
    };

    if !responses.has_key(&*trigger_key) {
        return "Such a trigger doesn't exist in the list specified".to_owned();
    }

    // //For now needs to be disabled, slash commands have quite a strict timelimit, so asking for affirmation times them out
    // let affirm_res = msg_channel
    // .say(
    // ctx,
    // format!(
    // "Are you sure you want to delete the trigger {}? (Y/N)",
    // trigger_key
    // ),
    // )
    // .await;
    //
    // if affirm_res.is_err() {
    // return "Failed affirm message send".to_owned();
    // }
    //
    // let answer = author.await_reply(&ctx).await;
    // return if let Some(message) = answer {
    // if message.content == "Y" {
    // responses.remove(&*trigger_key);
    //
    // if public_bool {
    // let _ = fs::write("responses.json", responses.dump());
    // } else {
    // let _ = fs::write("personal_user_responses.json", responses.dump());
    // }
    // format!("Done! {} was deleted", trigger_key)
    // } else if message.content == "N" {
    // "Cancelled".to_owned()
    // } else {
    // "Unknown input".to_owned()
    // }
    // } else {
    // "Cancelled".to_owned()
    // };

    responses.remove(&*trigger_key);

    if public_bool {
        let _ = fs::write("responses.json", responses.dump());
    } else {
        let _ = fs::write("personal_user_responses.json", responses.dump());
    }
    format!("Done! {} was deleted", trigger_key)
}

pub fn make_trigger(trigger_key: &str, response: &str, public_bool: bool, is_list: bool) -> String {
    let file_name = if public_bool {
        "responses.json"
    } else {
        "personal_user_responses.json"
    };

    let mut dict_triggers = json::parse(&*fs::read_to_string(file_name).unwrap()).unwrap();
    if is_list {
        dict_triggers[trigger_key.replace('+', " ")] =
            JsonValue::from(response.replace('+', " ").split(", ").collect::<Vec<_>>());
    } else {
        dict_triggers[trigger_key.replace('+', " ")] = JsonValue::from(response.replace('+', " "));
    }

    let _ = fs::write(file_name, dict_triggers.dump());

    format!("Set trigger {} with response {}", trigger_key, response)
}

pub fn show_triggers(public_bool: bool) -> String {
    let file_name = if public_bool {
        "responses.json"
    } else {
        "personal_user_responses.json"
    };

    if let Ok(x) = json::parse(&*fs::read_to_string(file_name).unwrap()) {
        "```json\n".to_owned() + &x.pretty(4) + "```"
    } else {
        "Couldn't read file".to_owned()
    }
}

pub async fn echo(
    ctx: &Context,
    channel_id: ChannelId,
    safe_passing: bool,
    text_to_echo: String,
) -> String {
    let channel_result = ctx.cache.channel(channel_id).await;
    let channel = if let Some(x) = channel_result {
        x
    } else {
        return "Couldn't find a channel with that id".to_owned();
    };

    let guild_channel_result = channel.guild();
    let guild_channel = if let Some(x) = guild_channel_result {
        x
    } else {
        return "Couldn't find a guild channel with that id".to_owned();
    };

    let echo_res = if safe_passing {
        let safe_text = content_safe(
            &ctx,
            text_to_echo,
            &ContentSafeOptions::new()
                .clean_everyone(true)
                .clean_here(true)
                .clean_role(true)
                .clean_channel(false)
                .clean_user(false),
        )
        .await;
        guild_channel.say(ctx, safe_text).await
    } else {
        guild_channel.say(ctx, text_to_echo).await
    };

    if echo_res.is_ok() {
        "Success".to_owned()
    } else {
        "Couldn't post echo".to_owned()
    }
}

pub fn test() -> String {
    "hiiiiii".to_owned()
}

pub async fn shutdown(command: ApplicationCommandInteraction, ctx: &Context) -> String {
    let data = ctx.data.read().await;

    let shard_manager = if let Some(v) = data.get::<ShardManagerContainer>() {
        v
    } else {
        return "There was a problem getting the shard manager".to_owned();
    };

    let _ = command
        .create_interaction_response(&ctx.http, |response| {
            response
                .kind(InteractionResponseType::ChannelMessageWithSource)
                .interaction_response_data(|message| message.content("Shutting down."))
        })
        .await; //Needs to be here, else shard shutdown prevents message send, which makes the interaction fail

    let owner = command.user;

    #[allow(clippy::print_stdout)]
    println!("Shutting down; requested by {} ({})", owner.name, owner.id);

    shard_manager.lock().await.shutdown_all().await;

    "Success".to_owned()
}

pub async fn ping(ctx: &Context) -> String {
    let data = ctx.data.read().await;

    let shard_manager = if let Some(v) = data.get::<ShardManagerContainer>() {
        v
    } else {
        return "There was a problem getting the shard manager".to_owned();
    };
    let manager = shard_manager.lock().await;

    let runners = manager.runners.lock().await;

    // Shards are backed by a "shard runner" responsible for processing events
    // over the shard, so we'll get the information about the shard runner for
    // the shard this command was sent over.
    let runner = if let Some(runner) = runners.get(&ShardId(ctx.shard_id)) {
        runner
    } else {
        return "No shard found???".to_owned();
    };

    let latency = if let Some(x) = runner.latency {
        x
    } else {
        return "Couldn't get latency, usually due to fresh start".to_owned();
    };

    return format!("The shard latency is {}ms", latency.as_millis());
}

pub async fn suggest(
    ctx: &Context,
    text_to_suggest: String,
    time: chrono::DateTime<Utc>,
    suggester: User,
) -> String {
    #[allow(clippy::unreadable_literal)]
    let channel_result = ctx.cache.channel(871400397225488404).await;
    let channel = if let Some(x) = channel_result {
        x
    } else {
        return "Couldn't find suggestions channel".to_owned();
    };

    let guild_channel_result = channel.guild();
    let guild_channel = if let Some(x) = guild_channel_result {
        x
    } else {
        return "Couldn't find a channel with that id".to_owned();
    };

    let guild_option = guild_channel.guild(ctx).await;
    let guild = if let Some(x) = guild_option {
        x
    } else {
        return "Couldn't get guild".to_owned();
    };

    let guild_member = if let Ok(x) = guild.member(ctx, suggester.id).await {
        x
    } else {
        return "Couldn't get guild user".to_owned();
    };
    let role_option = guild_member.highest_role_info(ctx).await;

    let role_colour = get_colour(role_option, &guild);

    let member_name = &guild_member.display_name();
    let avatar_endpoint = get_avatar_endpoint(&guild_member, &guild);

    //println!("{}", avatar_endpoint);
    //println!("{:?}", avatar_hash);

    let suggestion_res = guild_channel
        .send_message(ctx, |m| {
            m.embed(|e| {
                e.color(role_colour)
                    .field("**Suggestion:**", text_to_suggest, false)
                    .timestamp(time.to_rfc3339());

                e.thumbnail(avatar_endpoint)
            })
            .content(format!(
                "Sent by {} in {}",
                member_name,
                guild_channel.mention()
            ))
        })
        .await;

    if suggestion_res.is_ok() {
        "Success".to_owned()
    } else {
        "Failed to post suggestion".to_owned()
    }
}

pub async fn starboard(
    /*
    ctx: &Context,
    msg_channel: ChannelId,
     */
    author: User,
    in_bool_opt: Option<bool>,
) -> String {
    let opted_out_users = fs::read_to_string("starboard_opted_out_users.txt").unwrap();
    let already_opted_out = opted_out_users
        .split('|')
        .any(|user| user == author.id.to_string());

    if let Some(in_bool) = in_bool_opt {
        #[allow(clippy::collapsible_else_if)]
        if in_bool {
            if !already_opted_out {
                return "You're not opted out".to_owned();
            }

            /* //Need to comment these out is explained above
            let affirm_res = msg_channel
                .say(&ctx, "Are you sure you want to opt back in? (Y/N)")
                .await;

            if affirm_res.is_err() {
                return "Failed affirm message send".to_owned();
            }

            let answer = author.await_reply(&ctx).await;
            if let Some(message) = answer {
                return process_reply(&message, already_opted_out);
            };
             */
        } else {
            if already_opted_out {
                return "You're not opted in".to_owned();
            }

            /*
            //TO-DO
            let affirm_res = msg_channel.say(&ctx, "Your messages can currently be put on the starboard. Do you want to opt out? (Y/N)").await;

            if affirm_res.is_err() {
                return "Failed affirm message send".to_owned();
            }

            let answer = author.await_reply(&ctx).await;
            if let Some(message) = answer {
                return process_reply(&message, already_opted_out);
            };
            */
        }
        return process_reply_no_affirm(author.id, already_opted_out);
    }

    return process_reply_no_affirm(author.id, already_opted_out);
}
