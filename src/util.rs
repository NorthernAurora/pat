use serenity::model::guild::{Guild, Member};
use serenity::model::id::RoleId;
use serenity::utils::{Color, Colour};

pub fn get_avatar_endpoint(member: &Member, guild: &Guild) -> String {
    let mut avatar_endpoint = "https://cdn.discordapp.com/".to_owned();

    if let Some(hash) = &member.avatar {
        if hash.starts_with("a_") {
            avatar_endpoint.push_str(&format!(
                "guilds/{}/users/{}/avatars/{}.gif",
                &guild.id, &member.user.id, hash
            ));
        } else {
            avatar_endpoint.push_str(&format!(
                "guilds/{}/users/{}/avatars/{}.png",
                &guild.id, &member.user.id, hash
            ));
        }
    } else if let Some(hash) = &member.user.avatar {
        if hash.starts_with("a_") {
            avatar_endpoint.push_str(&format!("avatars/{}/{}.gif", &member.user.id, hash));
        } else {
            avatar_endpoint.push_str(&format!("avatars/{}/{}.png", &member.user.id, hash));
        }
    } else {
        avatar_endpoint.push_str(&format!(
            "embed/avatars/{}.png",
            &member.user.discriminator % 5
        ));
    }

    avatar_endpoint
}

pub fn get_colour(role_option: Option<(RoleId, i64)>, guild: &Guild) -> Colour {
    role_option.map_or_else(
        #[allow(clippy::unreadable_literal)]
        || Color::new(10070709),
        |x| {
            let (role_id, _) = x;
            let guild_role_option = guild.roles.get(&role_id);
            #[allow(clippy::unreadable_literal)]
            guild_role_option.map_or_else(|| Color::new(10070709), |x| x.colour)
        },
    )
}
