use serenity::prelude::*;

use crate::BOT_ADMIN_LIST;

use crate::commands::after_parsing::{
    del_trigger, echo, make_trigger, ping, show_triggers, shutdown, starboard, suggest, test,
};

use serenity::model::interactions::application_command::{
    ApplicationCommandInteraction, ApplicationCommandInteractionDataOptionValue,
};
use serenity::model::user::User;

pub async fn shutdown_parser(command: ApplicationCommandInteraction, ctx: &Context) -> String {
    if check_owner(&command.user) {
        shutdown(command, ctx).await
    } else {
        "You're not a bot admin, you can't do that.".to_owned()
    }
}

pub async fn suggest_parser(command: ApplicationCommandInteraction, ctx: &Context) -> String {
    let text_opt = command
        .data
        .options
        .get(0)
        .expect("Expected string option")
        .resolved
        .as_ref()
        .expect("Expected string object");

    if let ApplicationCommandInteractionDataOptionValue::String(text) = text_opt {
        let _ = suggest(ctx, text.clone(), command.id.created_at(), command.user).await;
        "Success".to_owned()
    } else {
        "Failure".to_owned()
    }
}

pub async fn echo_parser(command: ApplicationCommandInteraction, ctx: &Context) -> String {
    if !check_owner(&command.user) {
        return "You're not a bot admin, you can't do that.".to_owned();
    }

    let safe_pass_opt = command
        .data
        .options
        .get(0)
        .expect("Expected safe pass option")
        .resolved
        .as_ref()
        .expect("Expected safe pass object");
    let channel_opt = command
        .data
        .options
        .get(1)
        .expect("Expected channel option")
        .resolved
        .as_ref()
        .expect("Expected channel object");
    let trigger_key_opt = command
        .data
        .options
        .get(2)
        .expect("Expected echo text option")
        .resolved
        .as_ref()
        .expect("Expected echo text object");

    return if let ApplicationCommandInteractionDataOptionValue::Boolean(safe_pass) = safe_pass_opt {
        if let ApplicationCommandInteractionDataOptionValue::Channel(echo_channel) = channel_opt {
            if let ApplicationCommandInteractionDataOptionValue::String(echo_text) = trigger_key_opt
            {
                echo(ctx, echo_channel.id, *safe_pass, echo_text.clone()).await
            } else {
                "Couldn't get text to echo".to_owned()
            }
        } else {
            "Couldn't get channel to echo into".to_owned()
        }
    } else {
        "Couldn't get safe passing option".to_owned()
    };
}

pub fn make_trigger_parser(command: &ApplicationCommandInteraction) -> String {
    if !check_owner(&command.user) {
        return "You're not a bot admin, you can't do that.".to_owned();
    }

    let trigger_key_opt = command
        .data
        .options
        .get(0)
        .expect("Expected trigger option")
        .resolved
        .as_ref()
        .expect("Expected trigger object");
    let response_opt = command
        .data
        .options
        .get(1)
        .expect("Expected response option")
        .resolved
        .as_ref()
        .expect("Expected response object");
    let public_bool_opt = command
        .data
        .options
        .get(2)
        .expect("Expected public option")
        .resolved
        .as_ref()
        .expect("Expected public object");
    let is_list_opt = command
        .data
        .options
        .get(3)
        .expect("Expected list option")
        .resolved
        .as_ref()
        .expect("Expected list object");

    if let ApplicationCommandInteractionDataOptionValue::String(trigger_key) = trigger_key_opt {
        if let ApplicationCommandInteractionDataOptionValue::String(response) = response_opt {
            if let ApplicationCommandInteractionDataOptionValue::Boolean(public_bool) =
                public_bool_opt
            {
                if let ApplicationCommandInteractionDataOptionValue::Boolean(is_list) = is_list_opt
                {
                    make_trigger(trigger_key, response, *public_bool, *is_list)
                } else {
                    "Couldn't get valid option for if response is list".to_owned()
                }
            } else {
                "Couldn't get valid option for if response is public or private".to_owned()
            }
        } else {
            "Couldn't get valid response".to_owned()
        }
    } else {
        "Couldn't get valid trigger key".to_owned()
    }
}

pub async fn ping_parser(ctx: &Context) -> String {
    return ping(ctx).await;
}

pub fn test_parser() -> String {
    test()
}

pub async fn del_trigger_parser(command: ApplicationCommandInteraction, ctx: &Context) -> String {
    if !check_owner(&command.user) {
        return "You're not a bot admin, you can't do that.".to_owned();
    }

    let trigger_key_opt = command
        .data
        .options
        .get(0)
        .expect("Expected trigger option")
        .resolved
        .as_ref()
        .expect("Expected trigger object");
    let public_bool_opt = command
        .data
        .options
        .get(1)
        .expect("Expected public option")
        .resolved
        .as_ref()
        .expect("Expected public object");

    return if let ApplicationCommandInteractionDataOptionValue::String(trigger_key) =
        trigger_key_opt
    {
        if let ApplicationCommandInteractionDataOptionValue::Boolean(public_bool) = public_bool_opt
        {
            del_trigger(
                /*
                ctx,
                command.channel_id,
                command.user,
                */
                trigger_key.clone(),
                *public_bool,
            ) /*
              .await
              */
        } else {
            "Couldn't get valid option for if response is public or private".to_owned()
        }
    } else {
        "Couldn't get valid trigger key".to_owned()
    };
}

pub fn show_triggers_parser(command: &ApplicationCommandInteraction) -> String {
    if !check_owner(&command.user) {
        return "You're not a bot admin, you can't do that.".to_owned();
    }

    let public_bool_opt = command
        .data
        .options
        .get(0)
        .expect("Expected public option")
        .resolved
        .as_ref()
        .expect("Expected public object");

    if let ApplicationCommandInteractionDataOptionValue::Boolean(public_bool) = public_bool_opt {
        show_triggers(*public_bool)
    } else {
        "Couldn't get valid option for public or private list".to_owned()
    }
}

pub async fn starboard_parser(command: ApplicationCommandInteraction, ctx: &Context) -> String {
    let in_bool_opt = command.data.options.get(0);
    let in_bool_data;

    if in_bool_opt.is_some() {
        in_bool_data = in_bool_opt
            .expect("Should never ever trigger")
            .resolved
            .as_ref()
            .expect("Maybe could trigger");
    } else {
        return starboard(/*ctx, command.channel_id,*/ command.user, None).await;
    }

    return if let ApplicationCommandInteractionDataOptionValue::String(in_bool_text) = in_bool_data
    {
        if in_bool_text == "in" {
            starboard(/*ctx, command.channel_id,*/ command.user, Some(true)).await
        } else if in_bool_text == "out" {
            starboard(/*ctx, command.channel_id,*/ command.user, Some(false)).await
        } else {
            "Didn't get valid option for in/out".to_owned()
        }
    } else {
        starboard(/*ctx, command.channel_id,*/ command.user, None).await
    };
}

fn check_owner(user: &User) -> bool {
    if BOT_ADMIN_LIST.contains(user.id.as_u64()) {
        return true;
    }
    false
}
