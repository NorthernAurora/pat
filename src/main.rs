#![deny(rust_2018_idioms)]
#![deny(clippy::complexity, clippy::correctness, clippy::perf)]
#![warn(clippy::cargo, clippy::nursery, clippy::pedantic, clippy::style)]
#![allow(clippy::implicit_return, clippy::missing_docs_in_private_items)]
//Usually I've warned this, but since libraries I cannot satisfy the "multiple versions" lint so it's here
#![allow(clippy::multiple_crate_versions)]
//Same for this, usually warned, but now encountering too many places where there's no need to handle it
#![allow(clippy::let_underscore_drop)]
//Reactions blew past this fast
#![allow(clippy::too_many_lines)]

use std::collections::HashSet;
use std::fs::File;

use std::path::Path;
use std::sync::Arc;
use std::{env, fs};

use serenity::client::bridge::gateway::{GatewayIntents, ShardManager};
use serenity::framework::standard::macros::hook;
use serenity::model::channel::Message;

use serenity::model::id::UserId;
use serenity::{framework::standard::StandardFramework, http::Http, prelude::*};

mod commands;
mod event_handler;
mod interaction_parser;
mod util;

use commands::common::{ADMINISTRATIVE_GROUP, USER_GROUP};

use crate::event_handler::Handler;

use serenity::framework::standard::DispatchError;

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

pub struct FileContainer;

impl TypeMapKey for FileContainer {
    type Value = Arc<Mutex<File>>;
}

#[hook]
async fn dispatch_error_hook(context: &Context, msg: &Message, error: DispatchError) {
    if let DispatchError::OnlyForOwners = error {
        #[allow(clippy::let_underscore_drop)]
        let _ = msg
            .channel_id
            .say(&context, "You're not a bot admin, you can't do that.")
            .await;
    }
}

#[allow(clippy::unreadable_literal)] //Need to do it this way since slash commands don't have a way to check the owner param of the framework
static BOT_ADMIN_LIST: [u64; 3] = [839599306771398657, 805575602001281064, 198800494963982336];
static LIST_OF_RESPONSES_TO_PAT: [&str; 4] = [
    "<:meowmelt:861954709702705192>",
    "<:meowmaelt:863852672872218676>",
    "<:meowmewt:863852868058349579>",
    "yay!! Thank you!! <:admire:862678848008355860>",
];
static TRIGGERS_FOR_PATTER_BLUSH: [&str; 4] = [
    "<:meowpats:861950083779657728>",
    "good bot",
    "boop bot",
    "<a:patPat:871191644920250448>",
];

#[tokio::main]
async fn main() {
    let token = if Path::new("token.txt").is_file() {
        fs::read_to_string("token.txt").unwrap()
    } else {
        env::var("PAT_TOKEN").expect("Failed to get token from the environment.")
    };

    let http = Http::new_with_token(token.trim());

    let application_info = match http.get_current_application_info().await {
        Ok(info) => info,
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    let mut owners = HashSet::new();

    for id in BOT_ADMIN_LIST {
        owners.insert(UserId::from(id));
    }

    let app_id = application_info.id;

    let framework = StandardFramework::new()
        .configure(|c| {
            c.owners(owners)
                .prefix("pat!")
                .on_mention(Option::from(app_id))
        })
        .group(&ADMINISTRATIVE_GROUP)
        .group(&USER_GROUP)
        .on_dispatch_error(dispatch_error_hook);

    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .framework(framework)
        .application_id(u64::from(app_id))
        .intents(
            GatewayIntents::GUILDS
                | GatewayIntents::GUILD_MESSAGES
                | GatewayIntents::GUILD_MESSAGE_REACTIONS,
        )
        .await
        .expect("Error creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
    }

    let shard_manager = client.shard_manager.clone();

    tokio::spawn(async move {
        tokio::signal::ctrl_c()
            .await
            .expect("Could not register Ctrl+C handler");
        shard_manager.lock().await.shutdown_all().await;
    });

    if let Err(why) = client.start_autosharded().await {
        eprintln!("Client error: {:?}", why);
    }
}
