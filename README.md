A rust bot doing everything mentioned in the description. Written mostly safely, though makes assumptions in having the files it needs, at least as blanks. Those being "token.txt", "starboarded_messages.txt", "starboard_opted_out_users.txt", "responses.json", & "personal_user_responses.json". 

# Building & running

Only thing you really need is the [rust toolchain](https://www.rust-lang.org/learn/get-started). After which in the top level of the bot for a build run
    `cargo build`
  or for starting immediately
    `cargo run`
